/**
 * @file
 * Script with datalayer triggers implementations.
 */

(function ($, Drupal, drupalSettings) {
  "use strict";

  // Undefined string for type comparison.
  let TYPE_UNDEFINED = 'undefined',
  // Attributes for setting datalayer events.
      PREFIX_ATTR = 'data-attribute-datalayer-event-',
      CATEGORY_ATTR = PREFIX_ATTR + 'category',
      ACTION_ATTR = PREFIX_ATTR + 'action',
      LABEL_ATTR = PREFIX_ATTR + 'label',
      VALUE_ATTR = PREFIX_ATTR + 'value',
  // Selector to trigger datalayer on future.
      INLINE_LINK_CLASS = 'datalayer-event-click';

  /**
   * Generic function to send Datalayer data.
   *
   * @param {string} eventCategory
   *   Event Category item.
   * @param {string} eventAction
   *   Event Action item.
   * @param {string} eventLabel
   *   Event Label item.
   * @param {string} eventValue
   *   Event Value item.
   */
  function pushDatalayerClick(eventCategory, eventAction, eventLabel, eventValue) {
    dataLayer.push({
      'eventCategory': eventCategory,
      'eventAction': eventAction,
      'eventLabel': eventLabel,
      'eventValue': eventValue,
      'event': 'eventClick'
    });
  }

  /**
   * Get data from element and trigger DataLayer if possible.
   *
   * @param {string} element
   *   Element that triggered the event.
   */
  function triggerDatalayer(element) {
    var $element = $(element),
        category = $element.attr(CATEGORY_ATTR),
        action = $element.attr(ACTION_ATTR),
        label = $element.attr(LABEL_ATTR),
        value = $element.attr(VALUE_ATTR);

    // If Value is indefined, set default value.
    if (typeof value === TYPE_UNDEFINED) {
      value = '0';
    }

    // Trigger only if category, action, label exists.
    if ((typeof category !== TYPE_UNDEFINED) && (typeof action !== TYPE_UNDEFINED) && (typeof label !== TYPE_UNDEFINED)) {
      pushDatalayerClick(category, action, label, value);
    }
  }

  /**
   * Replace several placeholders for defining Datalayer.
   *
   * @param {string} originalText
   *   Original text with placeholders.
   * @param {object} $element
   *   Element with values to use as placeholder.
   * @param {object} selectors
   *   Selectors to get additional information.
   *
   * @return {string}
   *   Replaced text.
   */
  function replacePlaceholders(originalText, $element, selectors) {
    var elementText = $element.text(),
        elementVal = $element.val(),
        elementHref = $element.attr('href'),
        newText = originalText
          .replace('@text', elementText)
          .replace('@val', elementVal)
          .replace('@href', elementHref);

    if (selectors.find !== '') {
      var $find = $element.find(selectors.find),
          elementFindText = $find.text(),
          elementFindVal = $find.val(),
          elementFindHref = $find.attr('href');

      newText = newText
                  .replace('@find-text', elementFindText)
                  .replace('@find-val', elementFindVal)
                  .replace('@find-href', elementFindHref);
    }

    if (selectors.parents !== '') {
      var $parents = $element.parents(selectors.parents),
          elementParentsText = $parents.text(),
          elementParentsVal = $parents.val(),
          elementParentsHref = $parents.attr('href');

      newText = newText
                  .replace('@parents-text', elementParentsText)
                  .replace('@parents-val', elementParentsVal)
                  .replace('@parents-href', elementParentsHref);
    }

    if (selectors.find !== '' && selectors.parents !== '') {
      var $findParents = $element.find(selectors.find).parents(selectors.parents),
          elementFindParentsText = $findParents.text(),
          elementFindParentsVal = $findParents.val(),
          elementFindParentsHref = $findParents.attr('href'),
          $parentsFind = $element.parents(selectors.parents).find(selectors.find),
          elementParentsFindText = $parentsFind.text(),
          elementParentsFindVal = $parentsFind.val(),
          elementParentsFindHref = $parentsFind.attr('href');

      newText = newText
                  .replace('@find-parents-text', elementFindParentsText)
                  .replace('@find-parents-val', elementFindParentsVal)
                  .replace('@find-parents-href', elementFindParentsHref)
                  .replace('@parents-find-text', elementParentsFindText)
                  .replace('@parents-find-val', elementParentsFindVal)
                  .replace('@parents-find-href', elementParentsFindHref);
    }

    return newText;
  }

  /**
   * Set triggers defined by CMS.
   *
   * @param {object} trigger
   *   A trigger object with data to define datalayer.
   */
  function setDatalayerTrigger(trigger) {
    var $trigger = $(trigger.selector);

    $trigger.each(function (index, element) {
      var $element = $(element),
          hasDatalayerTrigger = $element.hasClass(INLINE_LINK_CLASS);

      // Avoid datalayer duplication.
      if (hasDatalayerTrigger) {
        return;
      }

      $element
        .addClass(INLINE_LINK_CLASS)
        .attr(CATEGORY_ATTR, replacePlaceholders(trigger.category, $element, trigger.aux_selectors))
        .attr(ACTION_ATTR, replacePlaceholders(trigger.action, $element, trigger.aux_selectors))
        .attr(LABEL_ATTR, replacePlaceholders(trigger.label, $element, trigger.aux_selectors))
        .attr(VALUE_ATTR, replacePlaceholders(trigger.value, $element, trigger.aux_selectors));
    });
  }

  /**
   * Drupal Behavior for binding events.
   */
  Drupal.behaviors.simpleDatalayerTrigger = {
    attach: function (context) {
      let ONCE_CLASS = 'datalayer-trigger';

      // Add attributes to all Datalayer Events.
      drupalSettings.simple_datalayer_trigger.triggers.forEach(setDatalayerTrigger);

      // Bind Links in CKEditor that should trigger datalayer.
      $('.' + INLINE_LINK_CLASS, context).once(ONCE_CLASS).on('click', function (event) {
        triggerDatalayer(this);
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
