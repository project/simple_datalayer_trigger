<?php

namespace Drupal\simple_datalayer_trigger\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DataLayerTriggerForm.
 */
class DataLayerTriggerForm extends EntityForm {

  /**
   * Jquery help text.
   */
  const JQUERY_HELP_TEXT = 'Selector to use with @link to get additional info.';

  /**
   * Helper function to print placeholder help text.
   *
   * @return string
   *   Translated HTML Markup.
   */
  private function getPlaceholderText() {
    $placeholders = [
      '<b>@text</b>: jQuery text() function',
      '<b>@val</b>: jQuery val() function',
      '<b>@href</b>: jQuery attr("href") function',
      '<b>@find-text</b>: jQuery find().text() functions',
      '<b>@find-val</b>: jQuery find().val() functions',
      '<b>@find-href</b>: jQuery find().attr("href") functions',
      '<b>@parents-text</b>: jQuery parents().text() functions',
      '<b>@parents-val</b>: jQuery parents().val() functions',
      '<b>@parents-href</b>: jQuery parents().attr("href") functions',
      '<b>@find-parents-text</b>: jQuery find().parents().text() functions',
      '<b>@find-parents-val</b>: jQuery find().parents().val() functions',
      '<b>@find-parents-href</b>: jQuery find().parents().attr("href") functions',
      '<b>@parents-find-text</b>: jQuery parents().find().text() functions',
      '<b>@parents-find-val</b>: jQuery parents().find().val() functions',
      '<b>@parents-find-href</b>: jQuery parents().find().attr("href") functions',
    ];
    $help_text = [
      $this->t('Use the following placeholders on the fields:'),
      '',
      '<ul><li>' . implode('</li><li>', $placeholders) . '</li></ul>',
      '',
    ];
    return implode('<br>', $help_text);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $datalayer_trigger = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $datalayer_trigger->label(),
      '#description' => $this->t("Label for the Data Layer trigger. Used only for admin purposes."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $datalayer_trigger->id(),
      '#machine_name' => [
        'exists' => '\Drupal\simple_datalayer_trigger\Entity\DataLayerTrigger::load',
      ],
      '#disabled' => !$datalayer_trigger->isNew(),
    ];

    $form['selector_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('JQuery Selectors'),
    ];

    $form['selector_container']['selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JQuery Compatible Selector'),
      '#maxlength' => 255,
      '#default_value' => $datalayer_trigger->get('selector'),
      '#description' => $this->t("JQuery compatible selector. It will be used to find the trigger element when DOM is complete."),
      '#required' => TRUE,
    ];

    $form['selector_container']['auxSelectorParents'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auxiliar selector with jQuery Parents'),
      '#maxlength' => 255,
      '#default_value' => $datalayer_trigger->get('auxSelectorParents'),
      '#description' => $this->t(self::JQUERY_HELP_TEXT, [
        '@link' => '<a href="https://api.jquery.com/parents/">JQuery Parents</a>',
      ]),
    ];

    $form['selector_container']['auxSelectorFind'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auxiliar selector with jQuery Find'),
      '#maxlength' => 255,
      '#default_value' => $datalayer_trigger->get('auxSelectorFind'),
      '#description' => $this->t(self::JQUERY_HELP_TEXT, [
        '@link' => '<a href="https://api.jquery.com/find/">JQuery Find</a>',
      ]),
    ];

    $form['datalayer_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Datalayer Content'),
    ];

    $form['datalayer_container']['help_text_1'] = [
      '#markup' => $this->getPlaceholderText(),
    ];

    $form['datalayer_container']['datalayerCategory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category'),
      '#maxlength' => 255,
      '#default_value' => $datalayer_trigger->get('datalayerCategory'),
      '#required' => TRUE,
    ];

    $form['datalayer_container']['datalayerAction'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Action'),
      '#maxlength' => 255,
      '#default_value' => $datalayer_trigger->get('datalayerAction'),
      '#required' => TRUE,
    ];

    $form['datalayer_container']['datalayerLabel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $datalayer_trigger->get('datalayerLabel'),
      '#required' => TRUE,
    ];

    $form['datalayer_container']['datalayerValue'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#maxlength' => 255,
      '#description' => $this->t("If empty, will consider default value."),
      '#default_value' => $datalayer_trigger->get('datalayerValue'),
    ];

    $form['datalayer_container']['help_text_2'] = [
      '#markup' => $this->getPlaceholderText(),
    ];

    $form['datalayer_container']['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['node', 'user'],
      '#show_restricted' => TRUE,
      '#global_types' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $datalayer_trigger = $this->entity;
    $status = $datalayer_trigger->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Data Layer trigger.', [
          '%label' => $datalayer_trigger->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Data Layer trigger.', [
          '%label' => $datalayer_trigger->label(),
        ]));
    }
    $form_state->setRedirectUrl($datalayer_trigger->toUrl('collection'));
  }

}
