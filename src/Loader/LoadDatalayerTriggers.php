<?php

namespace Drupal\simple_datalayer_trigger\Loader;

/**
 * Class Configuration.
 *
 * @package Drupal\simple_datalayer_trigger\Loader
 */
class LoadDatalayerTriggers {

  /**
   * Load all entities of Datalayer Trigger into a drupalSettings format.
   *
   * @return array
   *   Formated array.
   */
  public static function load() {
    $settings = [];
    $token = \Drupal::token();
    $triggers = \Drupal::entityTypeManager()->getStorage('datalayer_trigger')->loadMultiple();

    foreach ($triggers as $trigger) {
      $settings[] = [
        'selector' => $trigger->get('selector'),
        'aux_selectors' => [
          'parents' => $trigger->get('auxSelectorParents') ?: '',
          'find' => $trigger->get('auxSelectorFind') ?: '',
        ],
        'category' => $token->replace($trigger->get('datalayerCategory')),
        'action' => $token->replace($trigger->get('datalayerAction')),
        'label' => $token->replace($trigger->get('datalayerLabel')),
        'value' => $token->replace($trigger->get('datalayerValue') ?: '0'),
      ];
    }

    return $settings;
  }

}
