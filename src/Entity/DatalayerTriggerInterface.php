<?php

namespace Drupal\simple_datalayer_trigger\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Datalayer trigger entities.
 */
interface DatalayerTriggerInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
