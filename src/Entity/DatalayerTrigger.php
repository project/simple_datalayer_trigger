<?php

namespace Drupal\simple_datalayer_trigger\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Datalayer trigger entity.
 *
 * @ConfigEntityType(
 *   id = "datalayer_trigger",
 *   label = @Translation("Datalayer trigger"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\simple_datalayer_trigger\DatalayerTriggerListBuilder",
 *     "form" = {
 *       "default" = "Drupal\simple_datalayer_trigger\Form\DataLayerTriggerForm",
 *       "delete" = "Drupal\simple_datalayer_trigger\Form\DatalayerTriggerDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\simple_datalayer_trigger\DatalayerTriggerHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "datalayer_trigger",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/search/datalayer/datalayer_trigger/{datalayer_trigger}",
 *     "add-form" = "/admin/config/search/datalayer/datalayer_trigger/add",
 *     "edit-form" = "/admin/config/search/datalayer/datalayer_trigger/{datalayer_trigger}/edit",
 *     "delete-form" = "/admin/config/search/datalayer/datalayer_trigger/{datalayer_trigger}/delete",
 *     "collection" = "/admin/config/search/datalayer/datalayer_trigger"
 *   }
 * )
 */
class DatalayerTrigger extends ConfigEntityBase implements DatalayerTriggerInterface {

  /**
   * The Datalayer trigger ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Datalayer trigger label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Datalayer trigger Jquery Compatible Selector.
   *
   * @var string
   */
  protected $selector;

  /**
   * Aux Selector to search parents of selector.
   *
   * @var string
   */
  protected $auxSelectorParents;

  /**
   * Aux Selector to search forward of selector.
   *
   * @var string
   */
  protected $auxSelectorFind;

  /**
   * The Datalayer trigger Datalayer Category.
   *
   * @var string
   */
  protected $datalayerCategory;

  /**
   * The Datalayer trigger Datalayer Action.
   *
   * @var string
   */
  protected $datalayerAction;

  /**
   * The Datalayer trigger Datalayer Label.
   *
   * @var string
   */
  protected $datalayerLabel;

  /**
   * The Datalayer trigger Datalayer Value.
   *
   * @var string
   */
  protected $datalayerValue;

}
